#!/bin/bash
#
# Script Variables
# Shared variables for EGEM Quarry Node Setup scripts
#

# --------------------------------------------------------------------------

# go-egem latest version in GitLab
export git_goegem_version="1.1.2"

# file to save system information
export sysinfo="${egem_folder}/sysinfo"

# source branch of Go-EGEM repo
export goegem_branch="WIP_stuff"

# --------------------------------------------------------------------------

# minimum RAM requirement in MB
export min_ram="450"

# ideal amount of RAM
export ideal_ram="2048"

# url for external ip check
export ipcheck_url="ipinfo.io/ip"

# url for local Egem node verification
export egem_client_url="127.0.0.1:8897/stats"
