#!/bin/bash

# current script version
script_version="0.0.1"

# current setup goegem_branch
setup_branch="WIP_stuff"
env_URL="https://gitlab.com/luisvi70/test-linux-scripts/raw/master/egem-docker-setup/setup_environment.sh"

# EtherGem folder
export egem_folder="${HOME}/.ethergem"

collect_info(){
    we_have_all_we_need="true"

    # check if file exists,
    if [ -f ${sysinfo} ]; then
        source ${sysinfo} &> /dev/null

        # check each variable, collect system info again if there are any empty variables
        for x in ${os_name_long:-empty} ${os_name_short:-empty} ${cpu_arch:-empty} ${total_ram:-empty} ${total_disk:-empty}
        do
            if [ "${x}" == "empty" ]; then
                we_have_all_we_need="false"
                break
            fi
        done
    else
        we_have_all_we_need="false"
    fi

    case ${we_have_all_we_need} in
    "false")
        rm -rf ${sysinfo} &> /dev/null

        # -------------------------------------------------------
        # Collect "Operating System" information

            os_name_long=`lsb_release -d | tr -s '[:blank:]' ' ' | awk -F ':' '{$1="";print}' | xargs`

            echo 'export os_name_long="'${os_name_long}'"' >> ${sysinfo}

            if [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*8')" ]; then
                os_name_short="Debian_8"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*9')" ]; then
                os_name_short="Debian_9"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*10')" ]; then
                os_name_short="Debian_10"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'raspbian.*9')" ]; then
                os_name_short="Raspbian_9"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'raspbian.*10')" ]; then
                os_name_short="Raspbian_10"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'centos.*7')" ]; then
                os_name_short="CentOS_7"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*16')" ]; then
                os_name_short="Ubuntu_16"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*18')" ]; then
                os_name_short="Ubuntu_18"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*19')" ]; then
                os_name_short="Ubuntu_19"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*20')" ]; then
                  os_name_short="Ubuntu_20"
            else
                os_name_short="unidentified"
            fi

            echo 'export os_name_short="'${os_name_short}'"' >> ${sysinfo}

        # -------------------------------------------------------
        # Collect "CPU" information

            case `uname -m` in
            "armv6l"|"armv7l")
                cpu_arch="armv6l"
            ;;
            "aarch64")
                cpu_arch="arm64"
            ;;
            "x86"|"i386"|"i686"|"32bit"|"32 bit")
                cpu_arch="386"
            ;;
            "x86_64"|"64bit"|"64 bit")
                cpu_arch="amd64"
            ;;
            *)
                error "detect CPU architecture (current one is not supported)"
            ;;
            esac

            echo 'export cpu_arch="'${cpu_arch}'"' >> ${sysinfo}

        # -------------------------------------------------------
        # Collect "RAM" information

            total_ram=`free -m | grep -i "mem:" | awk '{print $2}'`
            echo 'export total_ram="'${total_ram}'"' >> ${sysinfo}

        # -------------------------------------------------------
        #
        # subtext "Disk"
        #
        #     total_disk=`df --output=target,size,avail -hl | grep '/' -w | awk '{print $2}'`
        #     echo 'export total_disk="'${total_disk}'"' >> ${sysinfo}
    ;;
    esac

    source ${sysinfo} &> /dev/null
}

show_status(){

    node_ip=`wget -q --no-check-certificate -O - ${ipcheck_url}`
    egem_client_version=`wget -q --no-check-certificate -O - ${egem_client_url} | sed 's/.*version\":\"\([0-9].[0-9].[0-9]\).*/\1/g'`

    echo "-------------------------------------------------------------------"
    echo " EGEM Quarry Node Setup   |  for more options use --help option"
    echo "-------------------------------------------------------------------"

    echo "      Setup Script Version     :  ${script_version}"
    echo "      Git Go-EGEM version      :  ${git_goegem_version}"
    echo
    echo "      Operating System         :  "${os_name_long}
    echo "      Public IP Address        :  ${node_ip}"
    echo

    if [ ! -z ${egem_client_version} ]; then
        echo "      Go-EGEM node status      :  working"
    else
        echo "      Go-EGEM node status      :  not working"
    fi
    echo "      Go-EGEM node version     :  ${egem_client_version:-unknown}"

    echo "-------------------------------------------------------------------"
    echo

    sleep 0.25s
}

help_text(){
    echo "-------------------------------------------------------------------"
    echo " EGEM Quarry Node Setup (v${script_version})"
    echo "-------------------------------------------------------------------"
    echo "    --help          -  displays this help text"
    echo "    --status        -  shows Quarry Node status"
    echo
    echo "    --install       -  installs EGEM Quarry Node (default)"
    echo "    --check         -  checks EGEM node installation"
    echo "    --update        -  updates EGEM node installation"
    echo "-------------------------------------------------------------------"
    echo

    exit 0
}


#
# Main script starts here
#

# Get script arguments
prm="${1}"

# Verify if EGEM folder exists
if [ ! -d ${egem_folder} ]; then
  mkdir ${egem_folder}
fi

# Check for setup_environment.sh file and execute
if [ ! -f "${egem_folder}/setup_environment.sh" ]; then
  echo "wget --no-check-certificate -O ${egem_folder}/setup_environment.sh ${env_URL}"
  wget --no-check-certificate -O ${egem_folder}/setup_environment.sh ${env_URL}
  if [ ! -f "${egem_folder}/setup_environment.sh" ]; then
    echo "Failed to get setup_environment.sh"
    exit 1
  fi
fi
source ${egem_folder}/setup_environment.sh

case ${prm} in
"--help")
    help_text
;;
"-v" | "v" | "--version" | "version")
    echo
    echo "EGEM Quarry Node Setup"
    echo "version: ${script_version}"
    echo

    exit 0
;;
esac

if [ $EUID -ne 0 ]; then
    echo
    echo "Error: This script must be run as root. Terminating..."
    echo
    exit 1
fi

case ${prm} in
"--status")
    collect_info
    show_status
    exit 0
;;
esac

exit 0 &> /dev/null
